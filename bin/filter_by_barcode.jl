#!/usr/bin/env julia
using ArgMacros, FASTX

function main()
	args = parse_arguments()

	barcodes = load_whitelist_barcodes(args.barcode_list)

	writers = (
        r1=FASTQ.Writer(auto_gzopen(args.output_r1; mode="w")),
        r2=FASTQ.Writer(auto_gzopen(args.output_r2; mode="w")),
    )
    readers = (
        r1=FASTQ.Reader(auto_gzopen(args.input_r1)),
        r2=FASTQ.Reader(auto_gzopen(args.input_r2)),
    )
	record = (r1=FASTQ.Record(), r2=FASTQ.Record())

	while !eof(readers.r1) && !eof(readers.r2)
        read!(readers.r1, record.r1)
        read!(readers.r2, record.r2)
	
		read_barcode = split(identifier(record.r1), ":")[end-1]

		if read_barcode in barcodes
			write(writers.r1, record.r1)
			write(writers.r2, record.r2)
		end
	end
end

function load_whitelist_barcodes(filepath::String)::Set{String}
    f = auto_gzopen(filepath)
    whitelist_barcodes = [strip(line) for line in eachline(f)]
    close(f)

    return Set{String}(whitelist_barcodes)
end

function parse_arguments()
	args = @tuplearguments begin
		@helpusage """
			filter_by_barcode.jl -i FASTQ_R1 -I FASTQ_R2 -o FASTQ_R1 -O FASTQ_R2
				-b BARCODE_FILE
		"""
		@helpdescription "Filters FASTQ files by cell barcode"

        @argumentrequired String input_r1 "-i" "--input-r1"
        @arghelp "FASTQ input file (R1, must have the barcodes)"
        @argtest input_r1 isfile "The input_r1 argument must be a valid file"

        @argumentrequired String input_r2 "-I" "--input-r2"
        @arghelp "FASTQ input file (R2)"
        @argtest input_r2 isfile "The input_r2 argument must be a valid file"

        @argumentrequired String output_r1 "-o" "--output-r1"
        @arghelp "FASTQ output file (R1)"

        @argumentrequired String output_r2 "-O" "--output-r2"
        @arghelp "FASTQ output file (R2)"

		@argumentrequired String barcode_list "-b" "--barcode-list"
		@arghelp "cell barcode whitelist (one per line)"
	end

	return args
end

if abspath(PROGRAM_FILE) == @__FILE__
    main()
end